/*
 * Routines to manage a trie, mapping string keys to arbitrary void *
 * payloads.
 */

#ifndef MSVC_EXTRACT_TRIE_H
#define MSVC_EXTRACT_TRIE_H

struct trie;

typedef void *(*trie_alloc_fn_t)(void *ctx);
typedef void (*trie_enum_fn_t)(void *ctx, const char *key, void *payload);

/*
 * Make a new empty trie.
 */
struct trie *trie_new(void);

/*
 * Ensure the string 'key' is in the trie. If it's not already there,
 * a new payload will be created for it by calling payload_new(ctx),
 * and returned. If the key is already there then payload_new will not
 * be called, and the return value will be whatever is already in the
 * trie.
 */
void *trie_insert(struct trie *tr, const char *key,
                  trie_alloc_fn_t payload_new, void *ctx);

/*
 * Return the payload (if any) associated with the string 'key'.
 */
void *trie_lookup(struct trie *tr, const char *key);

/*
 * Walk the whole trie, calling output_fn(ctx, key, payload) for each
 * entry.
 */
void trie_enumerate(struct trie *tr, trie_enum_fn_t output_fn, void *ctx);

#endif /* MSVC_EXTRACT_TRIE_H */
