#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "tar.h"

enum {
    TAR_TYPE_FILE = '0',
    TAR_TYPE_SYMLINK = '2',
    TAR_TYPE_DIR = '5'
};

union tar_hdr_block {
    struct {
        char file_name[100];
        char octal_file_mode[8];
        char octal_user[8];
        char octal_group[8];
        char octal_size[12];
        char octal_mod_time[12];
        char checksum[8];
        char file_type;
        char symlink_target[100];
        char ustar_signature[8];
        char name_user[32];
        char name_group[32];
        char major_number[8];
        char minor_number[8];
        char filename_prefix[155];
    } fields;
    unsigned char raw[512];
};

static void tar_header_init(union tar_hdr_block *hdr, int type,
                            const char *name, unsigned mode, size_t size)
{
    assert(sizeof(hdr->raw) >= sizeof(hdr->fields));
    memset(hdr->raw, 0, sizeof(hdr->raw));

    assert(strlen(name) < sizeof(hdr->fields.file_name));
    strcpy(hdr->fields.file_name, name);
    hdr->fields.file_type = type;
    sprintf(hdr->fields.octal_file_mode, "%.7o", mode);
    sprintf(hdr->fields.octal_user, "%.7o", 1000);
    sprintf(hdr->fields.octal_group, "%.7o", 1000);
    sprintf(hdr->fields.octal_size, "%.11lo", (unsigned long)size);
    sprintf(hdr->fields.octal_mod_time, "%.11o", 1483228800);
    strcpy(hdr->fields.ustar_signature, "ustar  ");
    strcpy(hdr->fields.name_user, "user");
    strcpy(hdr->fields.name_group, "group");
}

static void tar_header_finish(union tar_hdr_block *hdr)
{
    unsigned checksum;
    size_t i;

    memset(hdr->fields.checksum, ' ', sizeof(hdr->fields.checksum));
    checksum = 0;
    for (i = 0; i < sizeof(hdr->raw); i++)
        checksum += hdr->raw[i];
    sprintf(hdr->fields.checksum, "%.7o", checksum);
}

void tar_subdir(FILE *fp, const char *name)
{
    union tar_hdr_block hdr;
    tar_header_init(&hdr, TAR_TYPE_DIR, name, 0755, 0);
    assert(strlen(hdr.fields.file_name) < sizeof(hdr.fields.file_name) - 1);
    strcat(hdr.fields.file_name, "/");
    tar_header_finish(&hdr);
    fwrite(hdr.raw, sizeof(hdr), 1, fp);
}

void tar_symlink(FILE *fp, const char *name, const char *target)
{
    union tar_hdr_block hdr;
    tar_header_init(&hdr, TAR_TYPE_SYMLINK, name, 0777, 0);
    assert(strlen(target) < sizeof(hdr.fields.symlink_target));
    strcpy(hdr.fields.symlink_target, target);
    tar_header_finish(&hdr);
    fwrite(hdr.raw, sizeof(hdr), 1, fp);
}

void tar_file(FILE *fp, const char *name, const char *contents,
              int mode, size_t size)
{
    union tar_hdr_block hdr;
    char padding[512];
    size_t padlen;

    tar_header_init(&hdr, TAR_TYPE_FILE, name, mode, size);
    tar_header_finish(&hdr);
    fwrite(hdr.raw, sizeof(hdr), 1, fp);
    fwrite(contents, size, 1, fp);

    padlen = (-size) & 511;
    memset(padding, 0, padlen);
    fwrite(padding, padlen, 1, fp);
}

void tar_end(FILE *fp)
{
    char end[1024];
    memset(end, 0, sizeof(end));
    fwrite(end, sizeof(end), 1, fp);
}
