#include <windows.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <assert.h>
#include <ctype.h>

#include "tar.h"
#include "trie.h"

/*
 * This trie stores the set of case-correction symlinks we're going to
 * need.
 *
 * Payloads in this 'outer' trie are tries in their own right, mapping
 * a second layer of strings to null payloads. In other words, this
 * system of tries represents a map<string, map<string, unit>>, a.k.a.
 * a set of pairs of strings (but in such a way that it's easy to
 * iterate over all the secondary strings for a given primary one).
 *
 * Counterintuitively, the key in trie_outer is the thing we're going
 * to make symlinks _to_, and for a given such key, trie_inner stores
 * all the different symlinks to that thing we'll need.
 *
 * Both sets of keys are pathnames as they appear in #include
 * directives, not as they appear in the tar file. So they're relative
 * to _some_ directory on the include path, but at the time we read a
 * #include, we don't know which include path.
 *
 * For example, suppose we need the directive #include <Foo.h> to
 * work. Then we'll put the key "foo.h" into trie_outer (the real file
 * name in our output tar file), associated to an inner trie
 * containing the key "Foo.h". When we actually write the tar file,
 * we'll generate a symlink include_N/Foo.h -> foo.h for *every*
 * include_N directory that contains a header of that name (because
 * we're not sure which one the #include will be pointing at, and the
 * safe option is to make symlinks to them all).
 *
 * A more complicated example is that if we need #include <Foo/Bar.h>
 * to work, then we'll need symlinks include_N/foo/Bar.h -> bar.h and
 * include_N/Foo -> foo. In the trie representation, that translates to
 *
 *   trie_outer["foo/bar.h"] = "foo/Bar.h"
 *   trie_outer["foo"] = "Foo"
 */
static struct trie *trie_outer;

/*
 * This is a much simpler trie which just stores a set of subdirectory
 * names that are known to be architecture-specific. See the special
 * case commented in recurse_scan_path.
 */
static struct trie *known_arch_trie;

typedef void (*contents_scan_fn_t)(
    const char *buf, size_t size, const char *inpath);
typedef void (*output_modify_fn_t)(
    FILE *tarfp, const char *fullpath, const char *subpath);

static bool case_sensitive = true;

static const char *msc_ver = NULL;
static const char *platform = NULL;
static const char *clang_arch = NULL;
static const char *cmake_sysproc = NULL;

static void *trie_outer_new(void *ctx)
{
    return trie_new();
}

static void *trie_null_new(void *ctx)
{
    static int dummy;
    return &dummy;
}

static void check_trie_path(const char *filename_orig, size_t len);

/*
 * Scan function called when we iterate over header files for the
 * first time. Its job is to look through each header and find
 * #include directives, so that we know what non-lowercase names each
 * header file will need to be available under. Then it populates
 * trie_outer with the results.
 */
void headerscan(const char *buf, size_t size, const char *inpath)
{
    while (size > 0) {
        const char *fnstart;
        size_t len;

        while (size > 0 && *buf != '\n' && isspace((unsigned char)*buf))
            size--, buf++;
        if (!(size > 0 && *buf == '#'))
            goto no_match;
        size--, buf++;
        while (size > 0 && *buf != '\n' && isspace((unsigned char)*buf))
            size--, buf++;
        if (!(size >= 7 && !memcmp(buf, "include", 7)))
            goto no_match;
        size -= 7, buf += 7;
        while (size > 0 && *buf != '\n' && isspace((unsigned char)*buf))
            size--, buf++;
        if (!(size > 0 && (*buf == '"' || *buf == '<')))
            goto no_match;
        size--, buf++;
        fnstart = buf;
        while (size > 0 && *buf != '\n' && *buf != '"' && *buf != '>')
            size--, buf++;
	len = buf - fnstart;
        check_trie_path(fnstart, len);
      no_match:
        while (size > 0 && *buf != '\n')
            size--, buf++;
        if (size > 0)
            size--, buf++;
    }
}

static void check_trie_path(const char *filename_orig, size_t len)
{
    /* Make two mutable copies of filename_orig */
    char *filename = calloc(len + 1, 1);
    char *filename_lc = calloc(len + 1, 1);
    if (!filename || !filename_lc) {
        fprintf(stderr, "out of memory\n");
        exit(1);
    }
    memcpy(filename, filename_orig, len);

    /* Lowercase the one in filename_lc */
    size_t i;
    for (i = 0; i < len; i++) {
        char c = filename[i];
        if (!case_sensitive) {
            /* Treat all filenames as lower-case */
            filename[i] = tolower((unsigned char)c);
        }
        filename_lc[i] = c == '\\' ? '/' : tolower((unsigned char)c);
    }

    /* If they're not equal, we need to do some work */
    if (strcmp(filename_lc, filename)) {
        size_t slashpos;

        /*
         * Found a case in which a file is included by a name
         * that's not all lower case, so we'll need at least one
         * symlink.
         *
         * If the original filename also contains forward slashes,
         * then we'll need multiple symlinks. For example, an
         * attempt to #include <Foo/Bar.h> will need one symlink
         * Foo -> foo, and then inside the foo subdirectory,
         * another link Bar.h -> bar.h.
         *
         * However, if the original filename contains
         * _backslashes_, we have to put those directly into the
         * link filename. For example, #include <Foo\Bar.h> will
         * need a single symlink called 'Foo\Bar.h' pointing to
         * foo/bar.h.
         *
         * If there's a mixture, we have to match up each section.
         * So #include <Foo/Bar\Baz/Quux.h> would need:
         *
         * at top level:         Foo -> foo
         * inside foo:           Bar\Baz -> bar/baz
         * inside foo/bar/baz:   Quux -> quux.h
         */

        slashpos = (size_t)-1;
        while (1) {
            /*
             * Find the next '/' in the _original_ filename. These
             * are the boundaries at which we split it into sub-
             * symlinks.
             */
            size_t prevpos = ++slashpos;
            slashpos += strcspn(filename + prevpos, "/");

            /*
             * Save the character at that position, and
             * temporarily terminate both strings there.
             */
            char saved_filename_char = filename[slashpos];
            char saved_filename_lc_char = filename_lc[slashpos];
            filename[slashpos] = '\0';
            filename_lc[slashpos] = '\0';

            /*
             * If this particular component of the input filename
             * is already correctly cased and contains no
             * backslashes, then we don't need a link after all.
             */
            if (strcmp(filename + prevpos, filename_lc + prevpos)) {
                struct trie *trie_inner = trie_insert(
                    trie_outer, filename_lc, trie_outer_new, NULL);
                trie_insert(trie_inner, filename, trie_null_new, NULL);
            }

            /*
             * If we reached the real string terminator (even
             * before overwriting) in the original filename, we're
             * done.
             */
            if (!saved_filename_char)
                break;

            /*
             * Restore the characters we overwrote with string
             * terminators.
             */
            filename[slashpos] = saved_filename_char;
            filename_lc[slashpos] = saved_filename_lc_char;

            /*
             * And copy the new lowercase path segment from
             * filename_lc into filename, so that we'll create
             * further symlinks in output subdirectories that
             * really exist, not ones that are symlinks
             * themselves.
             */
            memcpy(filename + prevpos, filename_lc + prevpos,
                   slashpos - prevpos);
        }
    }
    free(filename);
    free(filename_lc);
}

static size_t position_after_last_char(const char *s, char c)
{
    const char *found = strrchr(s, c);
    return found ? found+1 - s : 0;
}

struct outputsymlink_ctx {
    FILE *tarfp;
    const char *linktarget;
    char *pathbuf;
    size_t filenamepos;
};

/*
 * Trie enumeration function that iterates over an inner trie,
 * generating symlinks to ctx->linktarget (derived from the key in the
 * outer trie).
 *
 * If ctx->linktarget is, for example, "foo/bar.h", then the keys here
 * will be things like "foo/Bar.h". So the actual link target we need
 * to write will not be ctx->linktarget itself, but a trailing
 * substring of it (here "bar.h").
 */
void outputsymlink(void *vctx, const char *key, void *payload)
{
    struct outputsymlink_ctx *ctx = (struct outputsymlink_ctx *)vctx;
    strcpy(ctx->pathbuf + ctx->filenamepos, key);
    size_t leafpos = position_after_last_char(key, '/');
    tar_symlink(ctx->tarfp, ctx->pathbuf, ctx->linktarget + leafpos);
}

/*
 * 'Modification' function that runs in the second scan_path pass over
 * the header files. After we write each non-symlink entry in the
 * output tarfile (a subdirectory or an actual header), this function
 * is called to generate any symlinks to the same entry. So it looks
 * up the pathname of the target file in trie_outer, and iterates over
 * the inner trie for that target.
 *
 * 'fullpath' will be the pathname relative to the tarfile's root
 * directory, starting with some include_N. 'subpath' will be the
 * pathname under that root dir (i.e. the key in trie_outer).
 */
void makesymlinks(FILE *tarfp, const char *fullpath, const char *subpath)
{
    struct trie *trie_inner = trie_lookup(trie_outer, subpath);
    if (trie_inner) {
        struct outputsymlink_ctx actx, *ctx = &actx;
        ctx->tarfp = tarfp;
        ctx->linktarget = subpath;

        /*
         * pathbuf starts off as the full path of the target file.
         * During enumeration, the part after the initial include_N
         * will be overwritten with the path of each symlink we're
         * making.
         */
        ctx->pathbuf = calloc(1 + strlen(fullpath), 1);
        if (!ctx->pathbuf) {
            fprintf(stderr, "out of memory\n");
            exit(1);
        }
        strcpy(ctx->pathbuf, fullpath);

        /*
         * filenamepos points to the place within pathbuf just after
         * the "include_N/" prefix: it's where we write the symlink
         * pathnames from the inner trie.
         */
        ctx->filenamepos = strlen(fullpath) - strlen(subpath);

        trie_enumerate(trie_inner, outputsymlink, ctx);
        free(ctx->pathbuf);
    }
}

void recurse_scan_path(FILE *tarfp, const char *inpath, const char *outpath,
                       size_t baselen,
                       contents_scan_fn_t scanfn, output_modify_fn_t modifyfn)
{
    HANDLE h;
    WIN32_FIND_DATA data;
    char *wildcard;

    if (tarfp) {
        tar_subdir(tarfp, outpath);
        if (modifyfn && strlen(outpath) > baselen)
            modifyfn(tarfp, outpath, outpath + baselen);
    }

    wildcard = calloc(strlen(inpath) + 3, 1);
    if (!wildcard) {
        fprintf(stderr, "out of memory\n");
        exit(1);
    }
    sprintf(wildcard, "%s\\*", inpath);

    h = FindFirstFile(wildcard, &data);
    free(wildcard);

    if (h == INVALID_HANDLE_VALUE) {
        fprintf(stderr, "FindFirstFile(\"%s\"): error %d\n",
                inpath, (int)GetLastError());
        exit(1);
    }

    do {
        size_t pos;
        char *pathname, *pathname_out;
        const char *filename = data.cFileName;
        if (!strcmp(filename, ".") || !strcmp(filename, ".."))
            continue;                  /* skip special subdirs */

	pathname = calloc(strlen(inpath) + strlen(filename) + 2, 1);
        pathname_out = calloc(strlen(outpath) + strlen(filename) + 2, 1);
        if (!pathname || !pathname_out) {
            fprintf(stderr, "out of memory\n");
            exit(1);
        }
        sprintf(pathname, "%s\\%s", inpath, filename);
        sprintf(pathname_out, "%s/%s", outpath, filename);
        for (pos = strlen(outpath) + 1; pathname_out[pos]; pos++)
            pathname_out[pos] = tolower((unsigned char)pathname_out[pos]);

        if (data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
            /*
             * Special case: we don't recurse into any subdirectory
             * matching a known architecture name. This avoids wasting
             * space on all the x64 and ARM libraries when we're
             * trying to archive the 32-bit x86 ones, because the
             * former are annoyingly kept in subdirectories next to
             * the latter.
             */
            const char *filename_lc = pathname_out + strlen(outpath) + 1;
            if (!trie_lookup(known_arch_trie, filename_lc))
                recurse_scan_path(tarfp, pathname, pathname_out,
                                  baselen, scanfn, modifyfn);
        } else {
	    FILE *fp;
            char *filedata;
            size_t filesize = data.nFileSizeHigh;
            filesize = filesize * ((size_t)1 + ~(DWORD)0) + data.nFileSizeLow;
            if ((filesize - data.nFileSizeLow) / 2 /
                ((size_t)1 + (~(DWORD)0)/2) != data.nFileSizeHigh) {
                fprintf(stderr, "file too large: %s\n", pathname);
            }

            filedata = calloc(filesize, 1);
            if (!filedata) {
                fprintf(stderr, "out of memory\n");
                exit(1);
            }
	    fp = fopen(pathname, "rb");
            if (fread(filedata, 1, filesize, fp) != filesize) {
                fprintf(stderr, "failed to read whole file: %s\n", pathname);
                exit(1);
            }
            fclose(fp);
            if (scanfn) {
                scanfn(filedata, filesize, pathname);
            }
            if (tarfp) {
                tar_file(tarfp, pathname_out, filedata, 0644, filesize);
                if (modifyfn) {
                    assert(strlen(pathname_out) > baselen);
                    modifyfn(tarfp, pathname_out, pathname_out + baselen);
                }
            }
            free(filedata);
        }

        free(pathname);
        free(pathname_out);
    } while (FindNextFile(h, &data));

    FindClose(h);
}

int scan_path(FILE *tarfp, const char *envvarname, const char *fnprefix,
              contents_scan_fn_t scanfn, output_modify_fn_t modifyfn)
{
    int index;
    const char *envvarval = getenv(envvarname);
    if (!envvarval || !*envvarval) {
        fprintf(stderr, "getenv(\"%s\"): not defined\n", envvarname);
        exit(1);
    }

    index = 0;
    while (*envvarval) {
        const char *semicolon;
        size_t pathlen, baselen;
        char *pathname, *outdirname;
        DWORD attrs;

	semicolon = envvarval + strcspn(envvarval, ";");
	pathlen = semicolon - envvarval;
	pathname = calloc(pathlen + 1, 1);
        memcpy(pathname, envvarval, pathlen);

        attrs = GetFileAttributes(pathname);
        if (attrs == INVALID_FILE_ATTRIBUTES ||
            !(attrs & FILE_ATTRIBUTE_DIRECTORY)) {
            fprintf(stderr, "skipping non-directory '%s' on %s path\n",
                    pathname, envvarname);
            free(pathname);
            goto advance;
        }

        outdirname = calloc(strlen(fnprefix) + 64, 1);
        if (!outdirname) {
            fprintf(stderr, "out of memory\n");
            exit(1);
        }
        baselen = 1 + sprintf(outdirname, "%s_%d", fnprefix, index);
        index++;

        recurse_scan_path(tarfp, pathname, outdirname,
                          baselen, scanfn, modifyfn);
        free(outdirname);

      advance:
        envvarval = semicolon;
        if (*envvarval)
            envvarval++;
    }

    return index;
}

size_t env_setup_script(char *scriptbuf, size_t scriptbufsize,
                        int ninclude, int nlib)
{
    size_t scriptlen = _snprintf(scriptbuf, scriptbufsize,
"#!/usr/bin/env python3\n"
"\n"
"import sys\n"
"import os\n"
"import shlex\n"
"\n"
"def makepath(prefix, limit):\n"
"    return \";\".join([os.path.join(\n"
"                os.path.dirname(os.path.abspath(__file__)),\n"
"                \"{}_{:d}\".format(prefix, i)) for i in range(limit)])\n"
"\n"
"def setenv(var, val):\n"
"    return \"{}={}; export {}\".format(var, shlex.quote(val), var)\n"
"\n"
"print(\"; \".join([\n"
"    setenv(\"INCLUDE\", makepath(\"include\", %d)),\n"
"    setenv(\"LIB\", makepath(\"lib\", %d)),\n"
"    setenv(\"Platform\", \"%s\"),\n"
"]))\n",
    ninclude, nlib, platform);
    if (scriptlen >= scriptbufsize) {
        fprintf(stderr, "setup script too long\n");
        exit(1);
    }
    return scriptlen;
}

size_t rust_link_wrapper(char *scriptbuf, size_t scriptbufsize, int nlib)
{
    size_t scriptlen = _snprintf(scriptbuf, scriptbufsize,
"#!/usr/bin/env python3\n"
"\n"
"import sys\n"
"import os\n"
"import shlex\n"
"\n"
"def makepath(prefix, limit):\n"
"    return \";\".join([os.path.join(\n"
"                os.path.dirname(os.path.dirname(os.path.abspath(__file__))),\n"
"                \"{}_{:d}\".format(prefix, i)) for i in range(limit)])\n"
"\n"
"os.environ[\"LIB\"] = makepath(\"lib\", %d)\n"
"os.execvp(\"lld-link\", sys.argv)\n",
    nlib);
    if (scriptlen >= scriptbufsize) {
        fprintf(stderr, "link script too long\n");
        exit(1);
    }
    return scriptlen;
}

size_t cmake_setup_script(char *scriptbuf, size_t scriptbufsize,
                          int ninclude, int nlib)
{
    size_t scriptlen = _snprintf(scriptbuf, scriptbufsize,
"#!/usr/bin/env python3\n"
"\n"
"import sys\n"
"import os\n"
"import shlex\n"
"import argparse\n"
"\n"
"msvcex_root = os.path.dirname(os.path.abspath(__file__))\n"
"\n"
"def makepath(prefix, limit):\n"
"    return \";\".join([os.path.join(\n"
"        msvcex_root, \"{}_{:d}\".format(prefix, i)) for i in range(limit)])\n"
"\n"
"def setenv(var, val):\n"
"    return \"{}={}; export {}\".format(var, shlex.quote(val), var)\n"
"\n"
"def main():\n"
"    parser = argparse.ArgumentParser(\n"
"        description=\"Make MSVC environment-setup wrappers and other setup \"\n"
"        \"files for LLVM tools\")\n"
"    parser.add_argument(\"toolsdir\",\n"
"                        help=\"bin directory containing toolchain to wrap\")\n"
"    parser.add_argument(\"-o\", \"--outdir\", default=msvcex_root,\n"
"                        help=\"root directory to put output files at\")\n"
"    args = parser.parse_args()\n"
"\n"
"    toolsdir = os.path.abspath(args.toolsdir)\n"
"    outdir = os.path.abspath(args.outdir)\n"
"    tools_to_wrap = [\"clang\", \"clang-cl\", \"lld-link\"]\n"
"\n"
"    def orig_path(toolname):\n"
"        return os.path.join(toolsdir, toolname)\n"
"    def wrapped_path(toolname):\n"
"        return os.path.join(outdir, \"bin\", toolname)\n"
"    def final_path(toolname):\n"
"        return (wrapped_path(toolname) if toolname in tools_to_wrap\n"
"                else orig_path(toolname))\n"
"\n"
"    try:\n"
"        os.mkdir(os.path.join(outdir, \"bin\"))\n"
"    except FileExistsError:\n"
"        pass\n"
"\n"
"    for toolname in tools_to_wrap:\n"
"        wrapperpath = wrapped_path(toolname)\n"
"        toolpath = orig_path(toolname)\n"
"        if not os.path.isfile(toolpath):\n"
"            sys.exit(\"Couldn't find tool {}\".format(toolpath))\n"
"        fd = os.open(wrapperpath,\n"
"                     os.O_CREAT | os.O_TRUNC | os.O_WRONLY, mode=0o755)\n"
"        with os.fdopen(fd, \"w\") as fh:\n"
"            print(\"#!/bin/sh\", file=fh)\n"
"            print(setenv(\"INCLUDE\", makepath(\"include\", %d)), file=fh)\n"
"            print(setenv(\"LIB\", makepath(\"lib\", %d)), file=fh)\n"
"            print(setenv(\"Platform\", \"%s\"), file=fh)\n"
"            print(\"exec {} \\\"$@\\\"\".format(shlex.quote(toolpath)), file=fh)\n"
"\n"
"    cmake_toolchain_path = os.path.join(outdir, \"toolchain.cmake\")\n"
"    with open(cmake_toolchain_path, \"w\") as fh:\n"
"        fh.write(f\"\"\"\n"
"# CMake toolchain file for cross-compiling for Windows using the\n"
"# clang/LLVM tools and the MSVC includes and headers.\n"
"#\n"
"# Autogenerated by msvc-extract.\n"
"\n"
"# Previous versions of cmake didn't get llvm-rc preprocessing right\n"
"cmake_minimum_required(VERSION 3.20)\n"
"\n"
"set(CMAKE_SYSTEM_NAME Windows)\n"
"if (NOT CMAKE_SYSTEM_VERSION)\n"
"  set(CMAKE_SYSTEM_VERSION 4.0)\n"
"endif()\n"
"set(CMAKE_SYSTEM_PROCESSOR %s)\n"
"\n"
"set(clang_cl_arch %s)\n"
"if (NOT clang_cl_msvc_version)\n"
"  set(clang_cl_msvc_version \"%s\")\n"
"endif()\n"
"set(clang_target ${{clang_cl_arch}}-pc-windows-msvc${{clang_cl_msvc_version}})\n"
"\n"
"set(CMAKE_C_COMPILER {final_path(\"clang-cl\")} --target=${{clang_target}})\n"
"set(CMAKE_CXX_COMPILER {final_path(\"clang-cl\")} --target=${{clang_target}})\n"
"set(CMAKE_LINKER {final_path(\"lld-link\")})\n"
"set(CMAKE_AR {final_path(\"llvm-lib\")})\n"
"set(CMAKE_MT {final_path(\"llvm-mt\")})\n"
"set(CMAKE_RC_COMPILER {final_path(\"llvm-rc\")})\n"
"\n"
"if(CMAKE_TOOLCHAIN_FILE)\n"
"  # Suppress spurious 'unused variable' warning about CMAKE_TOOLCHAIN_FILE\n"
"  # when regenerating makefiles in an existing CMake build directory\n"
"endif()\n"
"\"\"\")\n"
"\n"
"if __name__ == '__main__':\n"
"    main()\n",
    ninclude, nlib, platform, cmake_sysproc, clang_arch, msc_ver);
    if (scriptlen >= scriptbufsize) {
        fprintf(stderr, "setup script too long\n");
        exit(1);
    }
    return scriptlen;
}

void write_tar_to_stream(FILE *fp)
{
    int ninclude, nlib;
    char scriptbuf[16384];
    size_t scriptlen;

    /*
     * Pass 1 of include files: go through them looking for #include
     * directives to populate our trie of needed symlinks. No output
     * is generated to the tar file at this stage.
     */

    scan_path(NULL, "INCLUDE", "include", headerscan, NULL);

    /*
     * Pass 2 of include files: actually write them out to the tar
     * file, and after each output record, write any symlinks to it
     * that we need.
     */
    ninclude = scan_path(fp, "INCLUDE", "include", NULL, makesymlinks);

    /*
     * The libraries don't need scanning to find additional sources of
     * symlinks, so just write them straight to the tar in a single
     * pass.
     */
    nlib = scan_path(fp, "LIB", "lib", NULL, makesymlinks);

    /*
     * Write our auxiliary Python scripts into the tar file.
     */
    scriptlen = env_setup_script(scriptbuf, sizeof(scriptbuf),
                                 ninclude, nlib);
    tar_file(fp, "setup-sh.py", scriptbuf, 0755, scriptlen);
    scriptlen = cmake_setup_script(scriptbuf, sizeof(scriptbuf),
                                   ninclude, nlib);
    tar_file(fp, "setup-cmake.py", scriptbuf, 0755, scriptlen);
    scriptlen = rust_link_wrapper(scriptbuf, sizeof(scriptbuf), nlib);
    tar_subdir(fp, "rustbin");
    tar_file(fp, "rustbin/link.exe", scriptbuf, 0755, scriptlen);

    tar_end(fp);
}

void write_tar_to_file(const char *name)
{
    FILE *fp = fopen(name, "wb");
    if (!fp) {
        fprintf(stderr, "unable to open '%s'\n", name);
        exit(1);
    }
    write_tar_to_stream(fp);
    fclose(fp);
}

bool preprocess_to_integer(const char *source, unsigned *out)
{
    TCHAR tmpdir[MAX_PATH + 2];
    TCHAR tmpfile1[MAX_PATH + 2];
    TCHAR tmpfile2[MAX_PATH + 2];
    bool tmpfile1_created = false;
    bool tmpfile2_created = false;
    unsigned value = 0;
    bool toret = false;

    /* Prefix for temp file names, chosen once at random in
     * https://xkcd.com/221/ style to minimise collision with other
     * applications' temp file names */
    static const char tmpprefix[] = "muv";

    if (!GetTempPath(sizeof(tmpdir), tmpdir))
        goto out;

    if (!GetTempFileName(tmpdir, tmpprefix, 0, tmpfile1))
        goto out;
    tmpfile1_created = true;

    if (!GetTempFileName(tmpdir, tmpprefix, 0, tmpfile2))
        goto out;
    tmpfile2_created = true;

    FILE *fp = fopen(tmpfile1, "w");
    if (!fp)
        goto out;

    if (fputs(source, fp) == EOF)
        goto out;

    if (fclose(fp) == EOF)
        goto out;

    char command[sizeof(tmpfile1) + sizeof(tmpfile2) + 256];
    snprintf(command, sizeof(command), "cl /EP \"%s\" > \"%s\" 2>nul",
             tmpfile1, tmpfile2);
    if (system(command) != 0)
        goto out;

    fp = fopen(tmpfile2, "r");
    if (!fp)
        goto out;
    int c;
    while ((c = fgetc(fp)) != EOF) {
        if (c >= '0' && c <= '9')
            value = value * 10 + (c - '0');
    }
    fclose(fp);

    *out = value;
    toret = true;

  out:
    if (tmpfile1_created)
        remove(tmpfile1);
    if (tmpfile2_created)
        remove(tmpfile2);
    return toret;
}

const char *detect_msc_ver_from_cl(void)
{
    unsigned ver;
    if (!preprocess_to_integer("_MSC_FULL_VER\n", &ver))
        return NULL;

    static char msc_ver_buf[128];
    snprintf(msc_ver_buf, sizeof(msc_ver_buf), "%u.%u.%u",
             ver / 10000000, ver / 100000 % 100, ver % 100000);
    return msc_ver_buf;
}

const char *detect_platform_from_cl(void)
{
    unsigned id;
    if (!preprocess_to_integer("\
#if defined _M_AMD64\n\
2\n\
#elif defined _M_IX86\n\
1\n\
#elif defined _M_ARM64\n\
4\n\
#elif defined _M_ARM\n\
3\n\
#endif\n", &id))
        return NULL;

    if (id == 1)
        return "x86";
    if (id == 2)
        return "x64";
    if (id == 3)
        return "arm";
    if (id == 4)
        return "arm64";
    return NULL;
}

void usage(FILE *fp)
{
    fprintf(fp, "\
usage:   msvc-extract [options] <output-tar-file>\n\
options: -i              produce an archive without case-folding symlinks,\n\
                           suitable for use on a case-insensitive filesystem\n\
         --msc-ver=VER   override autodetection of compiler version for use\n\
                           in clang -fms-compatibility-version option\n\
         --aliases=FILE  file of extra pathnames that have to be case- and\n\
                           \\-mashed using symlinks\n\
also:    --help          display this text\n\
");
    exit(1);
}

static bool longopt_noval(const char *optstart, const char *optend,
                          const char *optname)
{
    size_t optlen = strlen(optname);
    if (!(optend == optstart + optlen && !strncmp(optstart, optname, optlen)))
        return false;                  /* option did not match */

    if (*optend) {
        fprintf(stderr, "msvc-extract: option '%s' does not expect a value\n",
                optname);
        exit(1);
    }

    return true;
}

static bool longopt_val(const char *optstart, const char *optend,
                        const char *optname, int *argcp, char ***argvp,
                        const char **optval)
{
    size_t optlen = strlen(optname);
    if (!(optend == optstart + optlen && !strncmp(optstart, optname, optlen)))
        return false;                  /* option did not match */

    if (*optend) {
        *optval = optend + 1;
    } else if (--(*argcp) > 0) {
        *optval = *++(*argvp);
    } else {
        fprintf(stderr, "msvc-extract: option '%s' expects a value\n",
                optname);
        exit(1);
    }

    return true;
}

static void read_aliases_file(const char *aliasfile)
{
    FILE *fp = fopen(aliasfile, "r");
    if (!fp) {
        fprintf(stderr, "msvc-extract: unable to open aliases file '%s'\n",
                aliasfile);
        exit(1);
    }

    char buf[4096];
    while (fgets(buf, sizeof(buf), fp)) {
        const char *p = buf;
        while (*p && isspace((unsigned char)*p))
            p++;
        if (!*p || *p == '#')
            continue;                  /* skip blank lines and comments */
        size_t len = strlen(p);
        while (len > 0 && (p[len-1] == '\r' || p[len-1] == '\n' ||
                           isspace((unsigned char)p[len-1])))
            len--;
        check_trie_path(p, len);
    }

    fclose(fp);
}

int main(int argc, char **argv)
{
    bool doing_opts = true;
    const char *aliasfile = NULL;
    const char *outfile = NULL;

    while (--argc > 0) {
        const char *arg = *++argv;

        if (doing_opts && arg[0] == '-' && arg[1]) {
            if (arg[1] == '-') {
                if (!arg[2]) {
                    doing_opts = false;
                    continue;
                }

                const char *optend = arg + strcspn(arg, "=");
                const char *optval;

                if (longopt_noval(arg, optend, "--help")) {
                    usage(stdout);
                    return 0;
                } else if (longopt_val(arg, optend, "--msc-ver",
                                       &argc, &argv, &optval)) {
                    msc_ver = optval;
                } else if (longopt_val(arg, optend, "--aliases",
                                       &argc, &argv, &optval)) {
                    aliasfile = optval;
                } else {
                    fprintf(stderr, "msvc-extract: unknown option '%.*s'\n",
                            (int)(optend-arg), arg);
                    return 1;
                }
            } else {
                const char *argpos = arg + 1;
                while (*argpos) {
                    char opt = *argpos++;
                    switch (opt) {
                      case 'i':
                        case_sensitive = false;
                        break;
                      default:
                        fprintf(stderr, "msvc-extract: "
                                "unknown option '-%c'\n", opt);
                        return 1;
                    }
                }
            }
        } else {
            if (!outfile) {
                outfile = arg;
            } else {
                fprintf(stderr, "msvc-extract: unexpected argument '%s'\n",
                        arg);
                return 1;
            }
        }
    }

    if (!outfile) {
        usage(stderr);
        return 1;
    }

    known_arch_trie = trie_new();
    trie_insert(known_arch_trie, "amd64", trie_null_new, NULL);
    trie_insert(known_arch_trie, "arm", trie_null_new, NULL);

    if (!msc_ver) {
        msc_ver = detect_msc_ver_from_cl();
        if (!msc_ver) {
            fprintf(stderr, "msvc-extract: unable to auto-detect MSVC version;"
                    " use --msc-ver=NN.NN.NNNNN to set it by hand\n");
            return 1;
        }
    }

    platform = getenv("Platform");
    if (!platform)
        platform = detect_platform_from_cl();

    if (platform && !_stricmp(platform, "x86")) {
        clang_arch = "i386";
        cmake_sysproc = "x86";
    } else if (platform && !_stricmp(platform, "x64")) {
        clang_arch = "x86_64";
        cmake_sysproc = "AMD64";
    } else if (platform && !_stricmp(platform, "arm")) {
        clang_arch = "arm";
        cmake_sysproc = "ARM";
    } else if (platform && !_stricmp(platform, "arm64")) {
        clang_arch = "aarch64";
        cmake_sysproc = "ARM64";
    } else if (platform) {
        fprintf(stderr, "unable to identify target architecture: "
                "Platform=\"%s\" not recognised\n", platform);
        exit(1);
    } else  {
        fprintf(stderr, "unable to identify target architecture from "
                "%%Platform%% or from cl.exe\n");
        exit(1);
    }

    trie_outer = trie_new();
    if (aliasfile)
        read_aliases_file(aliasfile);
    write_tar_to_file(outfile);
    return 0;
}
