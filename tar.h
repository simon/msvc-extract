/*
 * Routines to output a tar file directly to a stdio stream.
 */

#ifndef MSVC_EXTRACT_TAR_H
#define MSVC_EXTRACT_TAR_H

#include <stdio.h>

/*
 * Output a record that creates a subdirectory. 'name' should not end
 * with a slash (even though it will in the tar file itself).
 */
void tar_subdir(FILE *fp, const char *name);

/*
 * Output a record that creates a symlink. 'target' will be the
 * literal text stored in the link.
 */
void tar_symlink(FILE *fp, const char *name, const char *target);

/*
 * Output a record that creates a file. 'contents' and 'size' together
 * specify the content of the file; 'mode' is its permissions.
 */
void tar_file(FILE *fp, const char *name, const char *contents,
              int mode, size_t size);

/*
 * Output an end-of-tar-file record.
 */
void tar_end(FILE *fp);

#endif /* MSVC_EXTRACT_TAR_H */
