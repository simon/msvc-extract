SOURCES = msvc-extract.c tar.c trie.c
EXE = msvc-extract.exe
$(EXE): $(SOURCES)
	cl /Fe$(EXE) $(SOURCES)
