msvc-extract
============

This directory contains msvc-extract, a tool that archives the header
files and libraries from Visual Studio so that you can install them on
a Linux machine and use them with clang.cl.

Why?
----

Because then you can cross-compile your Windows programs on Linux.

This is very fast compared to doing it on Windows, partly because
Linux is generally nippier, and partly because of 'make -j', which is
better at parallelising builds than anything I've seen on Windows.

Also, it gives you at least some protection against malware, because
if malware wants to get on to your build machine and then infect the
binaries you're compiling, it would have to be cross-platform malware
capable of surviving on both Linux and Windows, and there's surely a
lot less of that around than the plain Windows-only kind.

How to build it
---------------

msvc-extract itself is a simple C program. You compile it on Windows,
using actual MSVC. (Or you could cross-compile it from Linux, I
suppose, but then, if you can do that already, you probably don't need
this program!)

The provided Makefile can be used with nmake, from a Visual Studio
command prompt. But it's extremely trivial; it would work just as well
to manually feed the three source files to a cl command, or load them
into the IDE, or whatever.

How to run it
-------------

msvc-extract takes one command-line argument, which is the name of a
tar file you want it to output.

You should run it in the context of a command prompt that's been set
up to run whatever instance of the Visual Studio command-line tools
you want clang-cl on Linux to mimic. It will pick up the settings from
your environment: %INCLUDE% and %LIB% pointing at the headers and
libraries, optionally %Platform% indicating the target platform, and
%PATH% pointing at the compiler itself (which msvc-extract will query
for its version number).

In the usual case, it should be enough to launch one of the special
command prompt shortcuts in the VS folder in the start menu, such as
"x64 Native Tools Command Prompt", or "x86 Native Tools Command
Prompt" (depending on whether you want to cross-compile for the 64-bit
or 32-bit instruction set). You can start one of those command
prompts, and inside it, change into the msvc-extract directory and run
commands such as

  nmake
  msvc-extract vs64.tar

If you want the libraries for Windows on Arm, then it's slightly more
difficult. First you'll have to compile msvc-extract itself to run on
the host Windows machine, using the native tools as above (just run
the 'nmake' command in a native-tools command prompt).

Then, start a fresh command prompt and find an appropriate setup
script for the cross-compiler. The exact location of these scripts
varies between Visual Studio versions. It may live at a location along
these lines (where the starting locations like %VS150COMNTOOLS% will
probably be defined in turn by the developer command prompt):

  "%VS140COMNTOOLS%"\..\..\vc\bin\amd64_x\vcvarsamd64_arm.bat
  "%VS150COMNTOOLS%"\..\..\vc\auxiliary\build\vcvarsamd64_arm64.bat
  "%VS160COMNTOOLS%"\..\..\vc\auxiliary\build\vcvarsamd64_arm64.bat

Having run one of those scripts, you can then run the same
msvc-extract command as before:

  msvc-extract output.tar

What to do with the output
--------------------------

The output of msvc-extract is a (large) uncompressed tar file. Copy
this over to a Linux machine (probably other Unixes will work too) and
unpack it via 'tar xf'.

It will generate a directory structure containing a bunch of numbered
include directories (include_0, include_1, ...), numbered library
directories (lib_0, lib_1, ...), and a file called setup-sh.py.

Running setup-sh.py will print out some shell commands to set up
environment variables called INCLUDE and LIB. If you feed those
commands in turn to your shell, e.g. by running

  eval "$(./setup-sh.py)"

then INCLUDE and LIB will be set up in such a way that compiling with
clang in MSVC mode (either the special clang-cl driver, or the normal
clang driver with an msvc architecture triple) will cause it to find
all the same include files, and lld-link to find all the same
libraries, that MSVC itself would have available. For example, after
using msvc-extract to get the x86-64 Windows headers and libraries,
and running the environment setup command printed by the resulting
setup-sh.py, you should be able to compile a hello-world program using
either of these commands, whichever you prefer:

  clang-cl --target=x86_64-pc-windows-msvc -fuse-ld=lld /Fehello.exe hello.c
  clang --target=x86_64-pc-windows-msvc -fuse-ld=lld -o hello.exe hello.c

Also in the root directory of the output, alongside setup-sh.py, is
another Python script called setup-cmake.py. That one only needs to be
run once, with an argument pointing at the bin directory of an LLVM
installation, for example

  ./setup-cmake.py /opt/llvm-11.0/bin

It will write out a file called 'toolchain.cmake', which can be used
with CMake-based software projects to automatically configure them to
build with the given LLVM toolchain and this set of include and header
files. The CMake variable you need to define is CMAKE_TOOLCHAIN_FILE.
For example, you might run (in a software project's source tree)
something like

  cmake . -DCMAKE_TOOLCHAIN_FILE=/some/path/to/toolchain.cmake

(with the '/some/path/to' replaced with wherever that file ended up).

Of course, not all cmake-based build systems will work first time with
that change. For example, if the project is not set up for cross-
compilation at all (e.g. because it compiles a preliminary program,
runs it, then uses the output to compile the main program), then the
project's own build files will also need modifying. But simple
projects should work out of the box like this.

Building Rust programs
----------------------

As well as C and C++ via cmake or ordinary makefiles, the tarball
output from this tool can also be used to cross-compile Rust programs
for Windows on a Linux build machine.

The Rust compiler can almost do the whole job already. If you add a
Windows build target using one of these commands ...

  rustup target add x86_64-pc-windows-msvc
  rustup target add aarch64-pc-windows-msvc

and then try to build a Rust project for one of those targets using
these commands ...

  cargo build --target=x86_64-pc-windows-msvc
  cargo build --target=aarch64-pc-windows-msvc

then the build process will do all the actual compiling, and only fail
at the link step, because when building for a '-windows-msvc' target,
it expects to be able to run the Visual Studio linker via 'link.exe'.

The tar file output from msvc-extract contains a 'rustbin'
subdirectory, containing an executable file called 'link.exe', which
is a wrapper on lld-link, enabling it to use the library files in the
same tarball.

So if you put the 'rustbin' directory on your PATH before running one
of those 'cargo build' commands, then cargo will use lld-link in place
of the real Visual Studio linker, and still be able to find all its
library files.

In full, a recipe I've found to work:

 - run an instance of the official 'rust' Docker image
 - install lld-link via 'apt update' and then 'apt install lld'
 - enable a Windows target in the Rust compiler, using one of the
   above rustup commands
 - unpack an msvc-extract tarball of the appropriate architecture for
   the Windows target you want to build for
 - add the 'rustbin' subdirectory of the result to your PATH
 - run the appropriate one of the above 'cargo build' commands. (Maybe
   also with --release.)

File and path naming issues
---------------------------

One problem with cross-compiling like this is the pathnames in
#include directives. On Windows, file names are case insensitive. So
it's legal for a source file to write something like

  #include <WiNdOwS.H>

and still get <windows.h>. Also, both / and \ are accepted as path
component separators in Windows #include directives.

Cross-compiling from Linux, clang won't compensate for that. So you
may find that you have to make your code base cross-compilation
friendly, by lowercasing the filenames in your #includes, and
normalising all the path separators to the Unix-style /.

The tar file output from msvc-extract provides every include file
under its lower-case name, so if you consistently use lower case and /
in all your #includes, everything should work.

If the code you're compiling insists on referring to headers or
libraries under pathnames that are not all lower-case, or that include
\ as a path separator instead of /, then msvc-extract has a
compensation mechanism. You can write a text file containing all the
pathnames that your particular code base will need to work, one per
line, and then pass the name of that text file to msvc-extract using
the --aliases option. Then your output tar file will contain symlinks
that allow the names you've specified to work as aliases for their
lowercase, Unix-style versions.

For example, if your program contains lines such as

  #include <CodeAnalysis\Warnings.h>

and its makefiles insist on referring to a library called AdvAPI.lib,
then you could write a file called (say) "aliases.txt", containing the
lines

  CodeAnalysis\Warnings.h
  AdvAPI.lib

and then run

  msvc-extract --aliases=aliases.txt output.tar

and the output tar file will contain symlinks that make those pathname
variants work.

(Some of the MSVC headers _themselves_ depend on filesystem case
insensitivity and \ path separators, because they include each other
using Windows-style pathnames. msvc-extract compensates for that by
scanning all the headers it puts in the tar file, noting any
non-lowercase names they use to refer to each other, and putting
appropriate symlinks into the tar file alongside the actual header
files. So cross-references between system headers should work without
needing this kind of user-provided configuration. But msvc-extract
can't account for mixed-case or mixed-slash includes in any source
code it hasn't scanned, unless you tell it what case combinations are
needed - it would be too expensive to include a symlink for every
possible case variation of every header.)

Colophon
--------

msvc-extract is free software, distributed under the MIT licence. See
LICENCE.txt for the full copyright notice and text of the licence.
