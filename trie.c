#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "trie.h"

struct trienode {
    struct trienode *children[256];
    void *payload;
};

struct trie {
    size_t maxlen;
    struct trienode head;
};

struct trie *trie_new(void)
{
    struct trie *tr = calloc(1, sizeof(struct trie));
    if (!tr) {
        fprintf(stderr, "out of memory\n");
        exit(1);
    }
    return tr;
}

static struct trienode *trie_new_node(void)
{
    struct trienode *node = calloc(1, sizeof(struct trienode));
    if (!node) {
        fprintf(stderr, "out of memory\n");
        exit(1);
    }
    return node;
}

static void *trie_walk(struct trie *tr, const char *key,
                       trie_alloc_fn_t payload_new, void *ctx)
{
    struct trienode *node = &tr->head;
    while (*key) {
        unsigned char child = *key++;
        if (!node->children[child]) {
            if (!payload_new)
                return NULL;
            node->children[child] = trie_new_node();
        }
        node = node->children[child];
    }
    if (!node->payload) {
        if (!payload_new)
            return NULL;
        node->payload = payload_new(ctx);
    }
    return node->payload;
}

void *trie_insert(struct trie *tr, const char *key,
                  trie_alloc_fn_t payload_new, void *ctx)
{
    size_t keylen = strlen(key);
    if (tr->maxlen < keylen)
        tr->maxlen = keylen;
    return trie_walk(tr, key, payload_new, ctx);
}

void *trie_lookup(struct trie *tr, const char *key)
{
    return trie_walk(tr, key, NULL, NULL);
}

static void trie_enumerate_recurse(
    struct trienode *node, char *outkey, size_t pos,
    trie_enum_fn_t output_fn, void *ctx)
{
    size_t child;

    if (node->payload) {
        outkey[pos] = '\0';
        output_fn(ctx, outkey, node->payload);
    }
    for (child = 0; child < 256; child++) {
        if (node->children[child]) {
            outkey[pos] = child;
            trie_enumerate_recurse(node->children[child], outkey, pos+1,
                                   output_fn, ctx);
        }
    }
}

void trie_enumerate(struct trie *tr, trie_enum_fn_t output_fn, void *ctx)
{
    char *outkey = calloc(tr->maxlen + 1, 1);
    if (!outkey) {
        fprintf(stderr, "out of memory\n");
        exit(1);
    }
    trie_enumerate_recurse(&tr->head, outkey, 0, output_fn, ctx);
    free(outkey);
}
